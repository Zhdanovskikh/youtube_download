FROM ubuntu:14.04
RUN apt–get update
RUN apt–get –y install MySQL-server
EXPOSE 3306
CMD ["/usr/bin/MySQLd_safe"]
